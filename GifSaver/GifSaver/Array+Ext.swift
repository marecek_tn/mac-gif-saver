//
//  Array+Ext.swift
//  GifSaver
//
//  Created by Marek Chrenko on 21/10/2016.
//  Copyright © 2016 monokell. All rights reserved.
//

import Foundation

extension Array {
    
    mutating func shuffle() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in startIndex ..< (endIndex - 1) {
            let j = Int(arc4random_uniform(UInt32(endIndex - i))) + i
            if i != j {
                swap(&self[i], &self[j])
            }
        }
    }
    
}
