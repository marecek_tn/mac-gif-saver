//
// Created by Marek Chrenko on 07/09/16.
// Copyright (c) 2016 monokell. All rights reserved.
//

import Foundation
import AVFoundation

struct Video {
    let url: URL
    var repeatCount: Int

    init(url: URL, repeatCount: Int?) {
        self.url = url

        if let rc = repeatCount {
            self.repeatCount = rc
        } else {
            //if not specified, repeat 4 times
            self.repeatCount = 4
        }
    }
}

class VideoLoader {

    static let sharedInstance = VideoLoader()

    private let configURL = "http://osd9000.monokell.com/playlist.php"
    private var loadedVideos: [Video] = []
    var notifyPlaylistLoaded: ((Void) -> Void)?

    fileprivate init() {
        loadConfig() {

            [weak self] videos in
            guard let ws = self else {
                NSLog("lost self instance during video loading")
                return
            }

            ws.loadedVideos = videos
            ws.notifyPlaylistLoaded?()
        }
    }

    func generateRandomPlaylist(count: Int) -> [AVPlayerItem] {
        return makePlaylist(pickRandomVideos(count: count), shuffle: true)
    }

    fileprivate func pickRandomVideos(count: Int) -> [Video] {
        if (loadedVideos.count == 0) || (count < 1) {
            return []
        }

        var usedIndices: [Int] = []
        var result: [Video] = []

        repeat {

            let randomIndex = Int(arc4random_uniform(UInt32(loadedVideos.count)))
            if usedIndices.contains(randomIndex) {
                continue
            }

            result.append(loadedVideos[randomIndex])
            usedIndices.append(randomIndex)

        } while usedIndices.count != min(count, loadedVideos.count)

        return result
    }

    fileprivate func makePlaylist(_ videos: [Video], shuffle: Bool) -> [AVPlayerItem] {

        var result: [AVPlayerItem] = []
        var preparedVideos = videos

        if shuffle {
            preparedVideos.shuffle()
        }

        for v in preparedVideos {
            for _ in 1 ... v.repeatCount {
                result.append(AVPlayerItem(url: v.url))
            }
        }

        return result
    }

    fileprivate func loadConfig(_ completion: @escaping ((_ items: [Video]) -> Void)) {

        guard let url = URL(string: configURL) else {
            print("error: invalid URL")
            return
        }

        let task = URLSession.shared.dataTask(with: url, completionHandler: {
            [weak self] data, response, error in

            guard let ws = self else {
                return
            }

            if let error = error {
                print("error: unable to load config: \(error)")
                return
            }

            if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
                print("error: unable to load config")
                return
            }

            if let data = data {
                completion(ws.parseData(data))
            } else {
                print("no data arrived")
            }

        })

        task.resume()
    }

    fileprivate func parseData(_ data: Data) -> [Video] {

        var result: [Video] = []

        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: AnyObject]

            guard let vids = json?["vids"] as? [[String: AnyObject]] else {
                print("no valid videos URL in json")
                return []
            }

            for vid in vids {
                if let url = vid["url"] as? String {
                    result.append(Video(url: URL(string: url)!, repeatCount: vid["repeat"] as? Int))
                }
            }

            return result

        } catch {
            print("error occurred parsing json: \(error)")
        }

        return []
    }

}
