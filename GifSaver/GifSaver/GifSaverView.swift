//
//  GifSaverView.swift
//  GifSaver
//
//  Created by Marek Chrenko on 06/09/16.
//  Copyright © 2016 monokell. All rights reserved.
//

import Foundation
import ScreenSaver
import AppKit
import AVKit
import AVFoundation

class GifSaverView: ScreenSaverView {

    static var queuePlayer = AVQueuePlayer()

    //used to sync video to all screens
    static var views: [GifSaverView] = []
    static var loaderInitialized = false
    private var playerLayer: AVPlayerLayer!

    override init?(frame: NSRect, isPreview: Bool) {
        super.init(frame: frame, isPreview: isPreview)
        initialize()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }

    deinit {
        NotificationCenter.default.removeObserver(GifSaverView.self)
    }

    fileprivate func initialize() {

        if !GifSaverView.loaderInitialized {
            let videoLoader = VideoLoader.sharedInstance
            videoLoader.notifyPlaylistLoaded = {
                GifSaverView.startPlaylist(videoLoader.generateRandomPlaylist(count: 1))
            }
            GifSaverView.loaderInitialized = true
        }


        setupPlayerLayer()

        GifSaverView.views.append(self)
        NotificationCenter.default.addObserver(GifSaverView.self, selector: #selector(GifSaverView.playerReachedEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
    }

    func setupPlayerLayer() {
        //setup showing layer of current view
        if self.layer == nil {
            self.layer = CALayer()
        }

        guard let layer = self.layer else {
            NSLog("error creating layer")
            return
        }
        self.wantsLayer = true
        layer.backgroundColor = NSColor.black.cgColor
        layer.needsDisplayOnBoundsChange = true
        layer.frame = self.bounds

        playerLayer = AVPlayerLayer(player: GifSaverView.queuePlayer)
        playerLayer.autoresizingMask = [CAAutoresizingMask.layerWidthSizable, CAAutoresizingMask.layerHeightSizable]
        playerLayer.frame = layer.bounds
        layer.addSublayer(playerLayer)
        layer.setNeedsDisplay()
    }

    class func startPlaylist(_ playlist: [AVPlayerItem]) {
        queuePlayer = AVQueuePlayer(items: playlist)
        queuePlayer.isMuted = true
        queuePlayer.actionAtItemEnd = .advance
        for v in views {
            v.setupPlayerLayer()
        }
        queuePlayer.play()
    }

    @objc static func playerReachedEnd(_ notification: Notification?) {

        if queuePlayer.items().count < 3 {
            //append generated playlist
            let generatedPlaylist = VideoLoader.sharedInstance.generateRandomPlaylist(count: 10)
            for movie in generatedPlaylist {
                queuePlayer.insert(movie, after: nil)
            }

        }
    }

}





