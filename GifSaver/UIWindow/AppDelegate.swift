//
//  AppDelegate.swift
//  UIWindow
//
//  Created by Marek Chrenko on 06/09/16.
//  Copyright © 2016 monokell. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!

    lazy var gifSaverView = GifSaverView(frame: NSZeroRect, isPreview: false)
    
    func applicationDidFinishLaunching(_ aNotification: Notification) {
        if let gifSaverView = gifSaverView {
            gifSaverView.frame = window.contentView!.bounds;
            window.contentView!.addSubview(gifSaverView);
        }
    }

}

